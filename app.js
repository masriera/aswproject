
var express         = require( 'express' ),
	bodyParser      = require( 'body-parser' ),
	cookieParser  	= require( 'cookie-parser' ),
	expressSession  = require( 'express-session'),
	pg     	        = require( 'pg' ),
	cors 			= require( 'cors' ),
	fs				= require( 'fs' ),
	passport 	    = require( 'passport' ),
	TwitterStrategy = require( 'passport-twitter' ).Strategy,
	usersCtrl       = require( './ctrl/ctrl.user.js' ),
	postCtrl        = require( './ctrl/ctrl.post.js' ),
	commentCtrl     = require( './ctrl/ctrl.comment.js' ),
	userMdl 		= require( './mdl/mdl.user.js' ),
	config          = require( './config.js' ),
	utils           = require( './utils/utils.app.js' ),
	app 	        = express(),
	publicPath      = __dirname + '/vw/public/';

// app conf
app.use( express.static( publicPath ) );
app.use( cookieParser() );
app.use( cors() );
app.use( bodyParser.json() );
app.use( expressSession({ secret: 'keyboard cat', resave: true, saveUninitialized: true }) );
app.use( passport.initialize() );
app.use( passport.session() );

// routes
app.get( '/', utils.renderHtml( publicPath + 'main.html' ) );

	// user
app.get( '/user/edit/:id', isLoggedIn, utils.renderHtml( publicPath + 'editUser.html' ) );
app.get( '/user', utils.renderHtml( publicPath + 'users.html' ) );

	// submit
app.get( '/submit/fullsubmit/', utils.renderHtml( publicPath + 'index.html' ) );
app.get( '/user/submit', utils.renderHtml( publicPath + 'submit.html' ) );
app.get( '/user/show/:id', utils.renderHtml( publicPath + 'userData.html' ) );
app.get( '/user/:id', usersCtrl.getUser );
app.delete( '/user/delete/:id', usersCtrl.deleteUser );
app.put( '/user/:id', isLoggedIn, usersCtrl.updateUser );
app.put( '/nolog/user/:id', usersCtrl.updateUser );

	// post
app.get( '/post/new', utils.renderHtml( publicPath + 'newPost.html' ) );
app.get( '/post/news/:id?', postCtrl.getPosts );
app.get( '/post/ask/:id?', postCtrl.getPosts );
app.get ('/post/getasks', utils.renderHtml( publicPath + 'getAsks.html' ) );
app.get( '/post', utils.renderHtml( publicPath + 'posts.html' ) );
app.get( '/post/edit/:id', utils.renderHtml( publicPath + 'editPost.html' ) );
app.get( '/post/fullpost/:id', utils.renderHtml( publicPath + 'post.html' ) );
app.get( '/post/:id', postCtrl.getPost);
app.delete( '/post/delete/:id', postCtrl.deletePost );
app.post( '/post/submit', isLoggedIn, postCtrl.newPost );
app.post( '/nolog/post/submit', postCtrl.newPost );
app.post( '/post/vote', isLoggedIn, postCtrl.votePost );
app.post( '/nolog/post/vote',  postCtrl.votePost );

	//comment
app.delete( '/comment/:id', commentCtrl.deleteComment );
app.post( '/comment/vote', isLoggedIn, commentCtrl.voteComment );
app.post( '/nolog/comment/vote', commentCtrl.voteComment );
app.get( '/comment/bypost/:postid:userid?', commentCtrl.postComments );
app.post( '/comment', isLoggedIn, commentCtrl.newComment );
app.post( '/nolog/comment', commentCtrl.newComment );
app.get( '/reply/byid/:commentid', commentCtrl.getComment );
app.get( '/reply/:postid:commentid', utils.renderHtml( publicPath + 'reply.html' ) );
app.post( '/reply/:parentid', commentCtrl.reply );

app.get( '/auth/twitter/images/favicon.ico', function( req, res ) {

	res.sendFile( publicPath + 'images/favicon.ico' );

});

function isLoggedIn( req, res, next ) {

	console.log( 'logged?', req.isAuthenticated() );

	req.isAuthenticated() ? next() : res.redirect( '/' );
}

/*twitter auth*/
passport.serializeUser( function( user, done ) {

	done( null, user );
});

passport.deserializeUser( function( obj, done ) {

	done( null, obj );
});

passport.use( new TwitterStrategy( config.auth.twitter, function( accessToken, refreshToken, profile, done ) {

	userMdl.getById( profile.id, function( err, user ) {

		if ( err ) {

		} else {

			if ( user ) {

				done( null, profile );

			} else {

				userMdl.create( profile.id, profile.username, function( err, result ) {

					done( null, profile );
				});
			}
		}
	});
}) );

app.get( '/auth/twitter', passport.authenticate('twitter') );
app.get( '/auth/twitter/callback/', passport.authenticate( 'twitter' ), function( req, res ) {

	var data = 'localStorage.username = "' + req.user.username + '"; localStorage.userid = ' + req.user.id + ';';

	res.status( 200 ).send( fs.readFileSync( publicPath +  'main.html', 'utf8' ).replace( '/*serverside*/', data ) );
});

app.get( '/logout', isLoggedIn, function( req, res ) {

	req.logout();
	res.redirect( '/' );
});

app.get( '*', utils.renderHtml( publicPath + 'main.html' ) );

// check db connection
pg.connect( config.db.url, function( err, client, done ) {
	
	done();
	
	if ( err ) {
		
		process.exit( 1 );
	}
	
	// run server
	app.listen( config.port, () => console.log( 'server listening at port ' + config.port ) );
});