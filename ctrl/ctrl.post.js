module.exports = ( function() {
    
    var postMdl = require( '../mdl/mdl.post.js' );
    var voteMdl = require( '../mdl/mdl.vote.js' );
    
    /** retrieves all the posts from the db
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function getPosts( req, res ) {
        
        var type = req.path.split('/')[2] === 'ask' ? 'ask' : 'new',
            userid = req.path.split('/')[3] ? req.path.split('/')[3]: '0';

        if ( userid === 'undefined' ) userid = null;

        postMdl.getAll( type, userid, function( err, post ) {
            
            var status = err ? 500 : 200,
                content = err || post;
            
            res.status( status ).json( content );
        });
    }
    
    /** retrieves a posts given its Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function getPost( req, res ) {
        
        var postId = req.params.id;
        
        postMdl.getById( postId, function( err, post ) {
            
            var status = err ? 500 : 200,
                content = err || post;
            
            res.status( status ).json( content );
        });
    }
    
    /** deletes a news_post given its Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function deletePost( req, res ) {
        
        var postId = req.params.id;
        
        postMdl.deleteById( postId, function( err ) {
            
            var status = err ? 500 : 200;
            
            res.status( status ).json( err );
        });
    }
    
    /** creates a new post 
     * required: title and url
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function newPost( req, res ) {

        console.log( 'POST' );
        console.log( req.body );

        var title = req.body.title,
            type = req.body.url ? 'new' : 'ask',
            url = req.body.url ? `'${req.body.url}'` : 'NULL',
            text = req.body.text ? `'${req.body.text}'` : 'NULL',
            userId = req.body.userId;

        postMdl.create( title, url, text, type, userId, function( err ) {

            var status = err ? 500 : 200;

            console.log( err );
            
            res.status( status ).json( err );
        });
    }

    /** updates a post
     *
     * @param {Object} req  Express request
     * @param {Object} res  Express response
     */
    function updatePost( req, res ) {

        var postId = req.params.id,
            updates = req.body;

        postMdl.update( postId, updates, function( err ) {

            var status = err ? 500 : 200;

            res.status( status ).json( err );
        });
    }

    /** create a vote
     *
     * @param {Object} req  Express request
     * @param {Object} res  Express response
     */
    function votePost( req, res ) {

        var postId = req.body.postId;
        var userId = req.body.userId;

        voteMdl.create( userId, postId, function( err ) {

            postMdl.votePostById( postId, function( err2 ) {

                var status = err2 ? 500 : 200;

                res.status( status ).json( err );
            });
        });
    }
    
    //exported
    return {
        getPosts   : getPosts,
        getPost    : getPost,
        deletePost : deletePost,
        newPost    : newPost,
        updatePost : updatePost,
        votePost   : votePost
    };
    
})();