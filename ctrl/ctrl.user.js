
module.exports = ( function() {
    
    var userMdl = require( '../mdl/mdl.user.js' );
    
    /** retrieves all the users from the db
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function getUsers( req, res ) {
        
        userMdl.getAll( function( err, users ) {
            
            var status = err ? 500 : 200,
                content = err || users;
            
            res.status( status ).json( content );
        });
    }
    
    /** retrieves a user given its Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function getUser( req, res ) {
        
        var userId = req.params.id;
        
        userMdl.getById( userId, function( err, user ) {
            
            var status = err ? 500 : 200,
                content = err || user;
            
            res.status( status ).json( content );
        });
    }
    
    /** deletes a user given its Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function deleteUser( req, res ) {
        
        var userId = req.params.id;
        
        userMdl.deleteById( userId, function( err ) {
            
            var status = err ? 500 : 200;
            
            res.status( status ).json( err );
        });
    }
    
    /** creates a new user 
     * required: name and password
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function newUser( req, res ) {
        
        var userName = req.body.name,
            password = req.body.psw; //TODO: cipher
        
        userMdl.create( userName, password, function( err ) {
            
            var status = err ? 500 : 200;
            
            res.status( status ).json( err );
        });
    }

    /** updates an user user
     *
     * @param {Object} req  Express request
     * @param {Object} res  Express response
     */
    function updateUser( req, res ) {

        var userId = req.params.id,
            updates = req.body;

        userMdl.update( userId, updates, function( err ) {

            var status = err ? 500 : 200;

            res.status( status ).json( err );
        });
    }
    
    //exported
    return {
        getUsers   : getUsers,
        getUser    : getUser,
        deleteUser : deleteUser,
        newUser    : newUser,
        updateUser : updateUser
    };
    
})();