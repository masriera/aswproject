
module.exports = ( function() {
    
    var commentMdl = require( '../mdl/mdl.comment.js' );
    var voteMdl = require( '../mdl/mdl.vote.js' );
    
    /** retrieves a list of comments from the db given its parent post Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function postComments( req, res ) {

       var postid = req.path.replace( '/comment/bypost/', '').split( '&' )[ 0 ],
           userid = req.path.replace( '/comment/bypost/', '').split( '&' )[ 1 ];
        commentMdl.getByPostId( postid, userid, function( err, comments ) {
            
            var status = err ? 500 : 200,
                content = err || comments;
            
            res.status( status ).json( content );
        });
    }
    
     /** creates a new comment 
     * required: post_id, parent_id, text
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function newComment( req, res ) {
        
        var postId = req.body.postid,
            userId = req.body.userid,
            text   = req.body.text;
        console.log(postId, userId, text)    
        commentMdl.create( postId, userId, text, function( err, post ) {

            console.log( arguments );

            var status = err ? 500 : 200,
                content = err || post;
            
            res.status( status ).json( content );
        });
    }
    
    /** retrieves a comment given its Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function getComment( req, res ) {
        
        var commentId = req.params.commentid;
        
        commentMdl.getById( commentId, function( err, comment ) {
            
            var status = err ? 500 : 200,
                content = err || comment;
            
            res.status( status ).json( content );
        });
    }
    
    /** retrieves all the comments from the db
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function getComments( req, res ) {
        
        var type = req.path === '/comment';
        
        commentMdl.getAll( type, function( err, comment) {
            
            var status = err ? 500 : 200,
                content = err || comment;
            
            res.status( status ).json( content );
        });
    }

    /** deletes a comment given its Id
     * 
     * @param {Object} req  Express request
     * @param {Object} res  Express response
    */
    function deleteComment( req, res ) {
        
        var commentId = req.params.id;
        
        commentMdl.deleteById( commentId, function( err ) {
            
            var status = err ? 500 : 200;
            
            res.status( status ).json( err );
        });
    }

    function reply( req, res ) {

        console.log( 'reply' );
        console.log( req.body );

        var parentId = req.params.parentid,
            postId = req.body.postid,
            text = req.body.text,
            user = req.body.userid;

        commentMdl.saveReply( parentId, postId, text, user, function( error, result ) {

            res.status( error ? 500 : 200 ).end();
        });
    }
    
    /** votes a comment
    *
    * @param {Object} req  Express request
    * @param {Object} res  Express response
    */
    /*function voteComment (req, res) {
        
        var commentId = req.params.id;
        console.log('ctrl', commentId);
        commentMdl.voteCommentById( commentId, function( err ) {

            var status = err ? 500 : 200;

            res.status( status ).json( err );
        });
    }*/

    /** create a vote
     *
     * @param {Object} req  Express request
     * @param {Object} res  Express response
     */
    function voteComment( req, res ) {
        
        var userId = req.body.userId;
        var commentId = req.body.commentId;
        var postId = req.body.postId;

        voteMdl.createComment( userId, postId, commentId, function( err ) {

            commentMdl.voteCommentById( commentId, function( err2 ) {

                var status = err2 ? 500 : 200;

                res.status( status ).json( err );
            });
        });
    }
    
    //exported
    return {
        postComments : postComments,
        newComment   : newComment,
        getComment   : getComment,
        getComments  : getComments,
        deleteComment: deleteComment,
        reply        : reply,
        voteComment  : voteComment
    };
    
})();