
module.exports = ( function() {

    var dbConfig = require( '../config.js' ).db;
        pg     = require( 'pg' );

    /** pg wrapper
     *
     * @param {String}      query      valid postgreSQL query
     * @param {Function}    callback
     */
    function makeQuery( query, callback ) {

        pg.connect( dbConfig.url, function( connectionErr, client, done ) {

            if ( connectionErr ) {

                console.log( connectionErr );
                callback( connectionErr, null );

            } else {

                client.query( query, function( err, result ) {

                    // releases pool connection
                    done();
                    callback( err, result );
                });
            }
        });
    }

    return {
        makeQuery : makeQuery
    };

})();