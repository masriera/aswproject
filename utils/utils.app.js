
module.exports = ( function() {

    /** sends to the client the file specified
     *
     * @param {String}      path
     * @returns {Function}  express send file wrapper
     */
    function renderHtml( path ) {

        return function( req, res ) {

            res.sendFile( path );
        }
    }

	return {
        renderHtml : renderHtml
    }

})();