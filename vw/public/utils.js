
// navbar content

function refreshNavbar() {

    var navBarLink = localStorage.username ?
        '<li><a id="username" href="/user/edit/' + localStorage.userid + '">' + localStorage.username + '</a></li><li><a id="logout">Log out</a></li>'
        : '<a href="/auth/twitter">Sign in with twitter</a>';

    $( '#rightNav').html( navBarLink );

    $( '#logout' ).on( 'click', function( e ) {

        localStorage.removeItem( 'userid' );
        localStorage.removeItem( 'username' );

        $.ajax({
            url         : '/logout',
            type        : 'GET',
            contentType : 'application/json'
        }).success( function( data ) {

            location.reload();
        })
        .fail( function( err ) {});
    });
}