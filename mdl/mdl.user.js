
module.exports = ( function() {
    
    var postgreSQL = require( '../utils/utils.db.js' );
    
    /** retrieves all the users
     * 
     * @param {Function} next   callback
    */
    function getAll( next ) {
        
        postgreSQL.makeQuery( 'SELECT * FROM public.user;' , function( err, result ) {

            var users = err ? null : result.rows;
            next( err, users );
        });
    }
    
    /** retrieves a user from the db given its Id
     * 
     * @param {Number}   userId   user identifier
     * @param {Function} next     callback
    */
    function getById( userId, next ) {
      
        postgreSQL.makeQuery( `SELECT * FROM public.user WHERE user_id=${userId};` , function( err, result ) {
          
            var user = err ? null : result.rows[ 0 ];
            next( err, user );
        });
    }

    /** inserts a user in the db
     * (name, password, timestamp)
     *
     * @param {String}   userName   name of the user
     * @param {String}   hash       hashed password
     * @param {Function} next       callback
    *
    function create( userName, hash, next ) {

        var query = `INSERT INTO public.user( name, password, created_at )  
                     VALUES( '${userName}', '${hash}', now() );`;
          
        postgreSQL.makeQuery( query, next );
    }*/
    function create( twitterId, twitterName, next ) {

        var query = `INSERT INTO public.user( user_id, name, created_at )
                     VALUES( '${twitterId}', '${twitterName}', now() );`;

        postgreSQL.makeQuery( query, next );
    }
    
    /** deletes a user from the db given its Id
     * 
     * @param {Number}   userId   user identifier
     * @param {Function} next     callback
    */
    function deleteById( userId, next ) {

        postgreSQL.makeQuery( `DELETE FROM public.user WHERE user_id=${userId};` , next );
    }

    /** updates the user specified
     *
     * @param {Number}   userId
     * @param {Object}   updates
     * @param {Function} next
     */
    function update( userId, updates, next ) {


        // json obj to sql update cols
        var updateString = Object
                .keys( updates )
                .reduce( function( pre, curr ) {

                    var value = updates[ curr ],
                        update = '';

                    if ( value ) {

                        update = `${curr}=` + ( typeof value === 'string' ? `'${ value }'` : `${value}`) + ',';
                    }

                     return pre + update;
                }, '' )
                .slice( 0, -1 ),
            query = `UPDATE public.user SET ${updateString} WHERE user_id=${userId};`;

        postgreSQL.makeQuery( query, next );
    }
    
    return {
        getAll     : getAll,
        getById    : getById,
        create     : create,
        deleteById : deleteById,
        update     : update
    };
    
})();