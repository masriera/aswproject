

module.exports = ( function() {
    
    var postgreSQL = require( '../utils/utils.db.js' );
    
    /** retrieves all the posts depending on its type
     * 
     * @param {String}  type    post type { 'ask'' | 'new' }
     * @param {Function} next   callback
    */
    function getAll( type, userid, next ) {

        var query = userid ?
                    `SELECT post.*, public.user.name, (SELECT count(*) FROM comment WHERE post.post_id = comment.post_id) AS comments,
                    (SELECT count(*) from vote where post.post_id = vote.post_id and vote.user_id = '${userid}')
                    AS vote
                    FROM post
                    INNER JOIN public.user
                    ON public.user.user_id = post.user_id
                    WHERE post.type = '${type}'
                    ORDER BY post.created_at DESC;`
            :
                `SELECT post.*, public.user.name, (SELECT count(*) FROM comment WHERE post.post_id = comment.post_id)
                    FROM post
                    WHERE post.type = '${type}'
                    ORDER BY post.created_at DESC;`;

        postgreSQL.makeQuery( query, function( err, result ) {

            var posts = err ? null : result.rows;
            next( err, posts );
        });
    }
    
    /** retrieves a post from the db given its Id
     * 
     * @param {Number}   postId   news post identifier
     * @param {Function} next     callback
    */
    function getById( postId, next ) {

        var query = `SELECT post.*, public.user.name FROM post
                    INNER JOIN public.user
                    ON post.post_id=${postId} AND post.user_id = public.user.user_id;`;

        postgreSQL.makeQuery( query, function( err, result ) {

            var post = err ? null : result.rows[ 0 ];
            next( err, post );
        });
    }

    /** inserts a post in the db
     * (title, url, text, timestamp)
     *
     * @param {String}   title      post's title
     * @param {String}   url        url that the post references
     * @param {String}   text       content of the ask post 
     * @params {String}  type       post type { 'ask'' | 'new' }   
     * @param {Function} next       callback
    */
    function create( title, url, text, type, user, next ) {

        var query = `INSERT INTO public.post( title, url, text, created_at, type, user_id )
                     VALUES( '${title}', ${url}, ${text}, now(), '${type}', ${user} );`;

        console.log( query );

        postgreSQL.makeQuery( query, next );
    }
    
    /** deletes a post from the db given its Id
     * 
     * @param {Number}   newsPostId   news post identifier
     * @param {Function} next     callback
    */
    function deleteById( postId, next ) {
      
      postgreSQL.makeQuery( `DELETE FROM public.post WHERE post_id=${postId};`, next );
    }

    /** updates the news post specified
     *
     * @param {Number}   postId
     * @param {Object}   updates
     * @param {Function} next
     */
    function update( postId, updates, next ) {

        // json obj to sql update cols
        var updateString = Object
                .keys( updates )
                .reduce( function( pre, curr ) {

                    var value = updates[ curr ],
                        update = '';

                    if ( value ) {

                        update = `${curr}=` + ( typeof value === 'string' ? `'${ value }'` : `${value}`) + ',';
                    }

                     return pre + update;
                }, '' )
                .slice( 0, -1 ),
            query = `UPDATE public.post SET ${updateString} WHERE post_id=${postId};`;

        postgreSQL.makeQuery( query, next );
    }
    
    function votePostById( postId, next ) {

        var query = `UPDATE public.post SET points = points + 1 WHERE post_id=${postId};`;

        postgreSQL.makeQuery( query, next );
    }
    
    
    return {
        getAll     : getAll,
        getById    : getById,
        create     : create,
        deleteById : deleteById,
        update     : update,
        votePostById : votePostById
    };
    
})();