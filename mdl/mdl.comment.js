
module.exports = ( function() {
    
    var postgreSQL = require( '../utils/utils.db.js' );
    
  
    
    /** retrieves a list of comments from the db given its  parent post Id
     * 
     * @param {Number}   postId   parent post identifier
     * @param {Function} next     callback
    */
    function getByPostId( postId, userid, next ) {

        if (userid) {
            var query = `SELECT comment.*, public.user.name,
                        (SELECT COUNT(*) from public.vote where vote.comment_id = comment.comment_id 
                        and vote.user_id = ${userid} ) as vote
                        FROM public.comment
                        INNER JOIN public.user
                        ON comment.post_id = ${postId} AND comment.user_id = public.user.user_id;`;
        }else {
            var query = `SELECT comment.*, public.user.name,
                        1 AS vote
                        FROM public.comment
                        INNER JOIN public.user
                        ON comment.post_id = ${postId} AND comment.user_id = public.user.user_id;`;
        }
                        

        postgreSQL.makeQuery( query , function( err, result ) {

            var comments = err ? null : result.rows;
            next( err, comments );
        });
    }

    /** deletes a comment from the db given its Id
     * 
     * @param {Number}   newsPostId   news post identifier
     * @param {Function} next     callback
    */
    function deleteById( commentId, next ) {
      
      postgreSQL.makeQuery( `DELETE FROM public.comment WHERE comment_id=${commentId};`, next );
    }


    /**
     *
     * @param commentId
     * @param next
     */
    function getById( commentId, next ) {

        var query = `SELECT comment.*, public.user.name FROM public.comment
                    INNER JOIN public.user
                    ON comment.comment_id = ${commentId} AND comment.user_id = public.user.user_id;`;

        postgreSQL.makeQuery( query , function( err, result ) {

            var post = err ? null : result.rows[ 0 ];
            next( err, post );
        });
    }

    /** creates a new comment given its post_id, user:_id and text 
     * 
     * @param {Number}   postId   parent post identifier
     * @param {Number}   userId   user identifier
     * @param {String}   text     content of the comment
     * @param {Function} next     callback
    */
    function create( postId, userId, text, next ) {

        var query = `INSERT INTO public.comment( post_id, user_id, text, created_at )  
                     VALUES( '${postId}', '${userId}', '${text}',  now() );`;

        console.log( query );

        postgreSQL.makeQuery( query, next );
    }

    /**
     *
     * @param parentId
     * @param postId
     * @param text
     * @param user
     * @param next
     */
    function saveReply( parentId, postId, text, user, next ) {

        var query = `INSERT INTO public.comment( post_id, parent_id, user_id, text, created_at )
                     VALUES( '${postId}', '${parentId}', '${user}', '${text}',  now() );`;

        postgreSQL.makeQuery( query, next );
    }


    function voteCommentById( commentId, next ) {

        var query = `UPDATE public.comment SET points = points + 1 WHERE comment_id=${commentId};`;

        postgreSQL.makeQuery( query, next );
    }
    
    return {
        getByPostId : getByPostId,
        getById     : getById,
        create      : create,
        saveReply   : saveReply,
        voteCommentById: voteCommentById,
        deleteById  : deleteById  
    };
    
})();