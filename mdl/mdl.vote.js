

module.exports = ( function() {
    
    var postgreSQL = require( '../utils/utils.db.js' );
    

    function create( userId, postId, next ) {

        var query = `INSERT INTO public.vote( user_Id, post_Id )
                     VALUES( '${userId}', ${postId});`;

        postgreSQL.makeQuery( query, next );
    }
    
    function createComment( userId, postId, commentId, next ) {
  
        var query = `INSERT INTO public.vote( user_Id, comment_Id )
                     VALUES( '${userId}', ${commentId});`;

        postgreSQL.makeQuery( query, next );
    }
  
    return {
       create        : create,
       createComment : createComment 
        
    };
})();